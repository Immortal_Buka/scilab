for i=1:20
    clear;
    b1 = 0/360;
    b2 = 180/360;
    a = b1:0.001:b2;
    b = ones(1,length(a));
    c = hsv2rgb(a,b,b);
    f = gcf(); 
    f.color_map = c;
    //scatter(a, a, 10, a*100, "fill");
    //a = size(gcf().color_map, 1);
    //Matplot(1:a);
    a = rand(1,1000);
    scatter(1.001:0.001:2, a, 10, a*1000, "fill");
    xgrid();
end;
