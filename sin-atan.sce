a = [0:0.01:2*%pi]';
f = atan(sin(a),cos(a));
g = atan(-sin(a),cos(a));
h = atan(sin(a),-cos(a));
i = atan(-sin(a),-cos(a));
plot2d(a,[a f g h i]);
legend('angle','atan(sin,cos)','atan(-sin,cos)','atan(sin,-cos)','atan(-sin,-cos)');
