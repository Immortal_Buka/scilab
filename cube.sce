/*
ax11+bx12+cx13+d=0;
ax21+bx22+cx23+d=0;
ax31+bx32+cx33+d=0;
ax41+bx42+cx43+d=0;
*/
clear;
x1 = 0;
x2 = 100;
x3 = 200;
x4 = 255;
y1 = 0;
y2 = 30;
y3 = 100;
y4 = 255;
m1=[
y1, x1^2, x1, 1; 
y2, x2^2, x2, 1; 
y3, x3^2, x3, 1; 
y4, x4^2, x4, 1];
m2=[
x1^3, y1, x1, 1; 
x2^3, y2, x2, 1; 
x3^3, y3, x3, 1; 
x4^3, y4, x4, 1];
m3=[
x1^3, x1^2, y1, 1; 
x2^3, x2^2, y2, 1; 
x3^3, x3^2, y3, 1; 
x4^3, x4^2, y4, 1];
m4=[
x1^3, x1^2, x1, y1; 
x2^3, x2^2, x2, y2; 
x3^3, x3^2, x3, y3; 
x4^3, x4^2, x4, y4];
md=[
x1^3, x1^2, x1, 1; 
x2^3, x2^2, x2, 1; 
x3^3, x3^2, x3, 1; 
x4^3, x4^2, x4, 1];
[e,m]=det(m1);
a = m*10^e;
[e,m]=det(m2);
b = m*10^e;
[e,m]=det(m3);
c = m*10^e;
[e,m]=det(m4);
d = m*10^e;
[e,m]=det(md);
e = m*10^e;
a = a/e;
b = b/e;
c = c/e;
d = d/e;
x=min(x1,x2,x3,x4);
y=max(x1,x2,x3,x4);
e=(y-x)/2;
x=[x-e:y+e];
y=a*x^3+b*x^2+c*x+d;
f = gcf(); 
plot2d(x,y);
f.color_map = autumncolormap(7);
x=[x1, x2, x3, x4];
y=[y1, y2, y3, y4];
i=1:1:length(x);
scatter(x, y, 20, i, "fill");
xtitle('a = ' + string(a) + ' b = ' + string(b) + ' c = ' + string(c) + ' d = ' + string(d));
