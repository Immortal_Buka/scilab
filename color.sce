clear;
ll = 10;
kkk = 14;
h = 1/ll:1/ll:1;//modulo(i,ll)/ll
s = ones(ll,1);
v = ones(ll,1);
kk = ones(1,kkk);
for i=1:ll
    [j(1),j(2),j(3)] = hsv2rgb(h(i), s(i), v(i));
    j = j*255;
    for k=1:3
        if j(k) < 16 then
            t(k) = '0' + string(dec2hex(round(j(k))));
        else
            t(k) = string(dec2hex(round(j(k))));
        end
    end
    for k=1:kkk
        cc(k) = '#' + t(1) + t(2) + t(3);
    end
    bar(i,kk,1,cc,'stacked');
end
a = gcf;
for i=1:ll
    b = gca().children(i).children.background;
    a.children.children(i).children.foreground = b;
    a.children.children(i).children.background = b;
end
