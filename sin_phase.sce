clear;
step = 0.01;
x=0:step:6.3;
i=3;
f=gcf(); 
f.color_map=autumncolormap(20);
if i==1 then
    z=zeros(length(x), length(x));
    a=1;
    for i=0:step:6.3
        z(:,a) = sin(x) - sin(x+i);
        a=a+1;
    end
    plot3d1(x,x,z);
elseif i==2 then
    z=zeros(length(x));
    y=zeros(length(x));
    a=1;
    for i=0:step:6.3
        z(a) = max(sin(x) - sin(x+i));
        y(a) = min(sin(x) - sin(x+i));
        a=a+1;
    end
    plot2d(x,z,1);
    plot2d(x,y,2);
    xgrid(21);  
elseif i==3 then
    plot2d(x,sin(7*x),1);
    plot2d(x,sin(9*x),11);
    xgrid(21);
else
    a=1;
    for i=0.2:0.2:4
        plot2d(x,sin(x)- sin(x+i),a);
        a=a+1;
    end
    xgrid(21);          
end
