max = 38;
x = 1:0.1:max;
y = max./x;
a = x+y;
plot2d(x,a,1);
plot2d(x,x,2);
plot2d(x,y,3);
xgrid(4);
xtitle(string(max) + ' -> '+string(ceil(min(a))));
