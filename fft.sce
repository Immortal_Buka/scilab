x = 0.01:0.01:25.6;
y = 1*sin(x+0.1);
for i=2:200
    y = y + (1-(i/200))*sin(x*i+0.1*i);
end
//y=1*sin(2*x+0.1)+1*sin(5*x+0.1)+1*sin(8*x+0.1)+1*sin(9*x+0.1)+1*sin(11*x+0.1);
//plot(x,y);
a=abs(fftw(y));
plot2d('nl', x(1:(length(x)/2)), a(1:(length(x)/2)), 2);
xgrid(1);
